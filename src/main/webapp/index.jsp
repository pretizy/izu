<%--
  Created by IntelliJ IDEA.
  User: GERALD OLEKA
  Date: 21/04/2016
  Time: 07:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <html ng-app="weatherApp">
    <head>
      <title>Hello AngularJS</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/sun.css">

        <!-- SPELLS -->
        <!-- load angular and angular route via CDN -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
        <script src="js/cold_effect.js"></script>
        <script src="js/app/main.js"></script>


    </head>

    <body>
    <!-- HEADER AND NAVBAR -->
    <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">Weather Info App</a>
                </div>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#/"><i class="fa fa-home"></i> Home</a></li>
                    <li><a href="#/about"><i class="fa fa-shield"></i> About</a></li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- MAIN CONTENT AND INJECTED VIEWS -->
    <div ng-view></div>

    </body>
    </html>