/**
 * Created by GERALD OLEKA on 21/04/2016.
 */


var API_URL= "http://localhost:8080/resources/weather";
var loadedBefore=false;




// create the module and name it weatherApp
var weatherApp = angular.module('weatherApp',  ['ngRoute']);

weatherApp.config(function($routeProvider, $locationProvider) {

    $routeProvider
        .when( '/', { controller: 'mainController', templateUrl: 'js/app/pages/home.html' } )
        .when('/about', { controller: 'aboutController', templateUrl: 'js/app/pages/about.html' })
        .otherwise( { redirectTo: '/' } );

     $locationProvider.html5Mode(false);
});

weatherApp.factory('appModel', [function () {
    return {
        model: {
            lat: '',
            lng: '',
            loading: false,
            hot: false,
            cold: false,
            temperature: "",
            observationTime: "",
            stationName:"",
            countryCode:"",
            description:false,
            errorMessage:""
}
    };
}]);



// create the controller and inject Angular's $scope
function mainController($scope, $http, appModel) {

    // create models in this controller scope to hold user input

    $scope.app =appModel;



    $scope.getWeatherInfo = function(){
        //clear possible error messages
        $scope.app.errorMessage="";
        //make loading gif show
        $scope.app.loading=true;
        //validate
        if(!isNaN($scope.app.lat) && !isNaN($scope.app.lng)) {
            //save cookies in browser storage
            $scope.app.lat= Math.abs($scope.app.lat);
            $scope.app.lng= Math.abs($scope.app.lng);
            setCookie("lat", $scope.app.lat + "", 50);
            setCookie("lng", $scope.app.lng + "", 50);

            //get the weather information
            $http({
                method: 'GET',
                url: API_URL + '/lat/' + $scope.app.lat + '/lng/' + $scope.app.lng
            }).then(function successCallback(response) {
                // when the response is available
                var data = response.data;
                if (data.temperature > 20) {
                    $scope.app.hot = true;
                    $scope.app.cold = false;
                } else {
                    $scope.app.cold = true;
                    $scope.app.hot = false;
                    cold_effect();
                }
                $scope.app.description = true;
                $scope.app.countryCode = data.description;
                $scope.app.temperature = data.temperature;
                $scope.app.stationName = data.stationName;
                $scope.app.observationTime = data.observationTime;
                $scope.app.loading = false;

            }, function errorCallback(response) {
                // or server returns response with an error status.
                $scope.app.errorMessage = "Error occurred while requesting from the server please try again";
                console.log(response.message)
                $scope.app.loading = false;
            });
        }else{
            $scope.app.errorMessage= "fill in both fields with a valid digit";
        }
    }

    //check if there are stored cookies for lat/lng and retrieve the last query again
    if(!loadedBefore) {
        if (getCookie("lat") != "" && getCookie("lng") != "") {
            $scope.app.lat = +getCookie("lat");
            $scope.app.lng = +getCookie("lng");
            $scope.getWeatherInfo($scope.app.lat, $scope.app.lng);
        }
        loadedBefore=true;
    }


};

function aboutController($scope) {

}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function cold_effect(){
    //canvas init
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");

    //canvas dimensions
    var W = window.innerWidth;
    var H = window.innerHeight;
    canvas.width = W;
    canvas.height = H;

    //snowflake particles
    var mp = 25; //max particles
    var particles = [];
    for(var i = 0; i < mp; i++)
    {
        particles.push({
            x: Math.random()*W, //x-coordinate
            y: Math.random()*H, //y-coordinate
            r: Math.random()*4+1, //radius
            d: Math.random()*mp //density
        })
    }

    //Lets draw the flakes
    function draw()
    {
        ctx.clearRect(0, 0, W, H);

        ctx.fillStyle = "rgba(255, 255, 255, 0.8)";
        ctx.beginPath();
        for(var i = 0; i < mp; i++)
        {
            var p = particles[i];
            ctx.moveTo(p.x, p.y);
            ctx.arc(p.x, p.y, p.r, 0, Math.PI*2, true);
        }
        ctx.fill();
        update();
    }

    //Function to move the snowflakes
    //angle will be an ongoing incremental flag. Sin and Cos functions will be applied to it to create vertical and horizontal movements of the flakes
    var angle = 0;
    function update()
    {
        angle += 0.01;
        for(var i = 0; i < mp; i++)
        {
            var p = particles[i];
            //Updating X and Y coordinates
            //We will add 1 to the cos function to prevent negative values which will lead flakes to move upwards
            //Every particle has its own density which can be used to make the downward movement different for each flake
            //Lets make it more random by adding in the radius
            p.y += Math.cos(angle+p.d) + 1 + p.r/2;
            p.x += Math.sin(angle) * 2;

            //Sending flakes back from the top when it exits
            //Lets make it a bit more organic and let flakes enter from the left and right also.
            if(p.x > W+5 || p.x < -5 || p.y > H)
            {
                if(i%3 > 0) //66.67% of the flakes
                {
                    particles[i] = {x: Math.random()*W, y: -10, r: p.r, d: p.d};
                }
                else
                {
                    //If the flake is exitting from the right
                    if(Math.sin(angle) > 0)
                    {
                        //Enter from the left
                        particles[i] = {x: -5, y: Math.random()*H, r: p.r, d: p.d};
                    }
                    else
                    {
                        //Enter from the right
                        particles[i] = {x: W+5, y: Math.random()*H, r: p.r, d: p.d};
                    }
                }
            }
        }
    }

    //animation loop
    setInterval(draw, 33);
}