package com.prognostore.izu.resources;

import com.prognostore.izu.model.WeatherInformation;
import com.prognostore.izu.services.WeatherService;
import org.geonames.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by GERALD OLEKA on 22/04/2016.
 * rest weather service class that handles client request
 */
@Path("weather")
public class WeatherResource {



    @GET
    @Path("/lat/{lat}/lng/{lng}")
    @Produces("application/json")
    public Response getWeatherInfo(@PathParam("lat")int lat, @PathParam("lng")int lng){

      try {
       WeatherInformation weatherInformation= new WeatherService().getWeatherInformation(lat, lng);
       return Response.ok().entity(weatherInformation).build();
      }catch (Exception e){
          return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
      }
    }



}
