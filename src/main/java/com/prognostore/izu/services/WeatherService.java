package com.prognostore.izu.services;

import com.prognostore.izu.model.WeatherInformation;
import org.geonames.WeatherObservation;
import org.geonames.WebService;

/**
 * Created by GERALD OLEKA on 22/04/2016.
 */
public class WeatherService {


    public WeatherInformation getWeatherInformation(int lat, int lng)throws Exception{
        //using the geonames java library to request the data
        WebService.setUserName("prognotest"); // add your username here

        WeatherObservation weatherObservation = null;
        weatherObservation = WebService.findNearByWeather(lat, lng);

        //jersey automatically converts to json
        WeatherInformation weatherInformation=new WeatherInformation();
        weatherInformation.setup(weatherObservation);
        return weatherInformation;
    }
}
