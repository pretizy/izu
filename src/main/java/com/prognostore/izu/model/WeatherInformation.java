package com.prognostore.izu.model;

import org.geonames.WeatherObservation;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by GERALD OLEKA on 22/04/2016.
 * object to return to web client
 */
@XmlRootElement
public class WeatherInformation {
    private String observation;
    private String observationTime;
    private String stationName;
    private String icaoCode;
    private String countryCode;
    private Integer elevation;
    private double latitude;
    private double longitude;
    private double temperature;
    private double dewPoint;
    private double humidity;
    private String clouds;
    private String weatherCondition;
    private String windSpeed;

    //constructor that fills model object with weather information
    public WeatherInformation() {
       
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getObservationTime() {
        return observationTime;
    }

    public void setObservationTime(String observationTime) {
        this.observationTime = observationTime;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getDewPoint() {
        return dewPoint;
    }

    public void setDewPoint(double dewPoint) {
        this.dewPoint = dewPoint;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public String getClouds() {
        return clouds;
    }

    public void setClouds(String clouds) {
        this.clouds = clouds;
    }

    public String getWeatherCondition() {
        return weatherCondition;
    }

    public void setWeatherCondition(String weatherCondition) {
        this.weatherCondition = weatherCondition;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public void setup(WeatherObservation weatherObservation) {
        this.observation = weatherObservation.getObservation();
        this.observationTime = weatherObservation.getObservationTime().toString();
        this.stationName = weatherObservation.getStationName();
        this.icaoCode = weatherObservation.getIcaoCode();
        this.countryCode = weatherObservation.getCountryCode();
        this.elevation = weatherObservation.getElevation();
        this.latitude = weatherObservation.getLatitude();
        this.longitude = weatherObservation.getLongitude();
        this.temperature = weatherObservation.getTemperature();
        this.dewPoint = weatherObservation.getDewPoint();
        this.humidity = weatherObservation.getHumidity();
        this.clouds = weatherObservation.getClouds();
        this.weatherCondition = weatherObservation.getWeatherCondition();
        this.windSpeed = weatherObservation.getWindSpeed();
    }
}
